# Gbd1m2p : Générateur de mot de passe à partir d'une phrase

## Introduction

`Gbd1m2p`, qui tire son nom de la phrase _"J'ai besoin d'un mot de passe"_, est une application qui permet de générer un mot de passe sécurisé et facile à retenir en utilisant une phrase fournie par l'utilisateur. L'objectif est de créer un mot de passe unique en extrayant les premières lettres de chaque mot, en appliquant des règles de substitution pour certains mots, et en gérant la casse pour renforcer la sécurité.

![](https://forge.apps.education.fr/lmdbt/gbd1m2p/-/raw/main/screenshot.png?ref_type=heads)

## Fonctionnement

L'utilisateur entre une phrase dans le champ de saisie. L'application traite cette phrase selon les règles suivantes pour générer le mot de passe :

- Prend la première lettre de chaque mot.
- Conserve les lettres en majuscule si elles le sont dans la phrase originale.
- Remplace certains mots par des caractères ou chiffres spécifiques :
  - "J'ai" et "j'ai" deviennent "G".
  - "Et" et "et" sont remplacés par "&".
  - "À" et "à" sont convertis en "@".
  - "Un" et "un" deviennent "1".
  - "D'un" et "d'un" se transforment en "D1" ou "d1".
  - "C'est" et "c'est" deviennent "C".
- Les caractères spéciaux tels que les tirets, points d'exclamation, virgules et points d'interrogation sont conservés à leur place dans le mot de passe généré.

## Critères de sécurité

L'application vérifie également que le mot de passe généré respecte les critères de sécurité suivants :

- Longueur supérieure à 12 caractères.
- Contient au moins une majuscule et une minuscule.
- Inclut un caractère spécial (comme @, &, -, , ou ?).
- Comprend au moins un chiffre.

## Exemples

- "J'ai besoin d'un mot de passe pour me connecter à Pronote." donne `Gbd1m2ppmc@P`.
- "À Paris, j'ai vu la tour Eiffel avec mes copains Bruno et Vincent." produit `@PGvltEamcB&V`.
- "Fabien a dit à Sonia : c'est fantastique!" donne `Fad@S:Cf!`.

## Auteur et Licence

Créé par Cyril Iaconelli, l'application est mise à disposition sous licence CC-BY. Plus d'informations et d'autres projets sont disponibles sur [www.lmdbt.fr](https://lmdbt.fr).

## Conclusion

Gbd1m2p est une solution pratique pour créer des mots de passe à la fois complexes et mémorables, en alliant la simplicité des phrases à la robustesse des mots de passe sécurisés.
